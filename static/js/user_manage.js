/***************************************************
 ** @Desc : This file for 用户管理js
 ** @Time : 2019.04.03 10:42
 ** @Author : Joker
 ** @File : user_manage
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.03 10:42
 ** @Software: GoLand
 ****************************************************/

let userManage = {
    add_user: function () {
        //添加后，查询条件置空
        let uMobile = $("#add_uMobile").val();
        let pay_fee = $("#pay_fee").val();
        let recharge_fee = $("#recharge_fee").val();
        let user_rate = $("#user_rate").val();

        let patrn = /^[1]([3-9])[0-9]{9}$/;
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

        if (uMobile === "") {
            toastr.error("手机号不能为空!");
        } else if (!patrn.exec(uMobile)) {
            toastr.error("请输入正确的手机号!");
        } else if (!patrn2.exec(pay_fee) || !patrn2.exec(recharge_fee) || !patrn2.exec(user_rate)) {
            toastr.error("请输入正确手续费或费率!");
        } else {

            $.ajax({
                type: "POST",
                url: "/user/add_user/",
                data: {
                    u_Mobile: uMobile,
                    u_Name: $("#add_uName").val(),
                    payFee: pay_fee,
                    rechargeFee: recharge_fee,
                    userRate: user_rate,
                    u_Type: $("#add_uType").val()
                },
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        $("#add_account_modal").modal('hide');
                        toastr.success("添加成功！");
                        setInterval(function () {
                            window.location.reload();
                        }, 3000);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    do_paging: function () {
        let uName = $("#uName").val();
        let uMobile = $("#uMobile").val();
        let uType = $("#uType").val();
        let u_Status = $("#uStatus").val();
        $.ajax({ //去后台查询第一页数据
            type: "GET",
            url: "/user/list/",
            data: {
                page: '1',
                limit: "20",
                uName: uName,
                uMobile: uMobile,
                uType: uType,
                uStatus: u_Status,
            }, //参数：当前页为1
            success: function (data) {
                if (data.root != null) {
                    userManage.show_user_data(data.root);//处理第一页的数据

                    let options = {//根据后台返回的分页相关信息，设置插件参数
                        bootstrapMajorVersion: 3, //
                        currentPage: data.page, //当前页数
                        totalPages: data.totalPage, //总页数
                        numberOfPages: data.limit,//每页记录数
                        itemTexts: function (type, page) {//设置分页按钮显示字体样式
                            switch (type) {
                                case "first":
                                    return "首页";
                                case "prev":
                                    return "上一页";
                                case "next":
                                    return "下一页";
                                case "last":
                                    return "末页";
                                case "page":
                                    return page;
                            }
                        },
                        onPageClicked: function (event, originalEvent, type, page) {//分页按钮点击事件
                            $.ajax({//根据page去后台加载数据
                                url: "/user/list/",
                                type: "GET",
                                data: {
                                    page: page,
                                    uName: uName,
                                    uMobile: uMobile,
                                    uType: uType,
                                    uStatus: u_Status,
                                },
                                success: function (data) {
                                    userManage.show_user_data(data.root);//处理数据
                                }
                            });
                        }
                    };
                    $('#xf_page').bootstrapPaginator(options);//设置分页
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    show_user_data: function (list) {
        let con = "";
        $.each(list, function (index, item) {
            let uType = "普通用户";
            switch (item.Authority) {
                case 100:
                    uType = "管理员";
                    break;
                case 101:
                    uType = "VIP用户";
                    break;
                case 102:
                    uType = "普通用户";
                    break;
                case 103:
                    uType = "对接用户";
                    break;
            }

            con += "<tr><td><a>" + (index + 1) + "</a></td>";
            con += "<td>" + item.UserName + "</td>";
            con += "<td>" + item.Mobile + "</td>";
            con += "<td>" + item.TotalAmount.toFixed(2) + "</td>";
            con += "<td>" + item.UsableAmount.toFixed(2) + "</td>";
            con += "<td>" + item.FrozenAmount.toFixed(2) + "</td>";
            con += "<td>" + uType + "</td>";
            con += "<td>" + item.EditTime + "</td>";
            con += "<td><button type=\"button\" class=\"btn btn-primary btn-xs\" onclick=\"userManage.edit_user_ui(" + item.LoginName + ");\" title=\"编辑\"><i class=\"fa fa-pencil\"></i>编辑</button>";
            con += "&nbsp;<button type=\"button\" class=\"btn btn-info btn-xs\" onclick=\"userManage.look_user(" + item.LoginName + ");\" title=\"查看\"><i class=\"glyphicon glyphicon-zoom-in\"></i>查看</button>";
            con += "&nbsp;<button type=\"button\" class=\"btn btn-warning btn-xs\" onclick=\"userManage.addition_user_ui(" + item.LoginName + ");\" title=\"加款\"><i class=\"glyphicon glyphicon-arrow-up\"></i>加款</button>";
            con += "&nbsp;<button type=\"button\" class=\"btn btn-warning btn-xs\" onclick=\"userManage.deduction_user_ui(" + item.LoginName + ");\" title=\"减款\"><i class=\"glyphicon glyphicon-arrow-down\"></i>减款</button>";
            con += "&nbsp;<button type=\"button\" class=\"btn btn-xs\" onclick=\"userManage.reset_user(" + item.LoginName + ");\" title=\"重置密码\"><i class=\"glyphicon glyphicon-erase\"></i>重置密码</button>";

            if (item.Status === 0) {
                con += "&nbsp;<button type=\"button\" class=\"btn btn-danger btn-xs\" onclick=\"userManage.freeze_user(" + item.LoginName + ");\" title=\"冻结\"><i class=\"fa fa-times-circle\"></i>冻结</button>";
            } else {
                con += "&nbsp;<button type=\"button\" class=\"btn btn-success btn-xs\" onclick=\"userManage.active_user(" + item.LoginName + ");\" title=\"激活\"><i class=\"fa fa-user\"></i>激活</button>";
            }

            con += "</td></tr>";
        });
        $("#your_showtime").html(con);
    },
    edit_user_ui: function (edit_id) {
        $.ajax({
            type: "GET",
            url: "/user/edit_user_ui/" + edit_id,
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    $("#edit_label_name").html(res.data.u.UserName);
                    $("#edit_LName").val(res.data.u.LoginName);
                    $("#edit_uMobile").val(res.data.u.Mobile);
                    $("#edit_uName").val(res.data.u.UserName);
                    $('#edit_uType').val(res.data.u.Authority);
                    $('#edit_pay_fee').val(res.data.u.PayFee);
                    $('#edit_recharge_fee').val(res.data.u.RechargeFee);
                    $('#edit_user_rate').val(res.data.u.RechargeRate);

                    $('#edit_ips').val(res.data.ux.Ips);

                    $('#edit_account_modal').modal({backdrop: 'static', keyboard: false});
                } else {
                    toastr.error("此用户不存在！");
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    edit_user: function () {
        let pay_fee = $("#edit_pay_fee").val();
        let recharge_fee = $("#edit_recharge_fee").val();
        let user_rate = $("#edit_user_rate").val();
        let edit_ips = $("#edit_ips").val();

        let patrn = /^[1]([3-9])[0-9]{9}$/;
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;
        let patrn3 = /(2(5[0-5]{1}|[0-4]\d{1})|[0-1]?\d{1,2})(\.(2(5[0-5]{1}|[0-4]\d{1})|[0-1]?\d{1,2})){3}/g;

        let ip = edit_ips.split(";");

        let edit_uMobile = $("#edit_uMobile").val();
        let edit_uName = $("#edit_uName").val();
        if (!patrn.exec(edit_uMobile)) {
            toastr.error("请输入正确的手机号!");
        } else if (!patrn2.exec(pay_fee) || !patrn2.exec(recharge_fee) || !patrn2.exec(user_rate)) {
            toastr.error("请输入正确手续费或费率!");
        } else if (ip[0] !== "") {
            if (!patrn3.exec(ip)) {
                toastr.error("请输入正确的IP!");
            } else {
                $.ajax({
                    type: "POST",
                    url: "/user/edit_user/",
                    data: {
                        uMobile: edit_uMobile,
                        uName: edit_uName,
                        payFee: pay_fee,
                        rechargeFee: recharge_fee,
                        userRate: user_rate,
                        IPs: edit_ips,
                        uType: $("#edit_uType").val()
                    },
                    cache: false,
                    success: function (res) {
                        if (res.code === 9) {
                            $("#edit_account_modal").modal('hide');
                            toastr.success("更新成功！");
                            setInterval(function () {
                                window.location.reload();
                            }, 3000);
                        } else {
                            toastr.error(res.msg);
                        }
                    },
                    error: function (XMLHttpRequest) {
                        toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                    }
                });
            }
        }
    },
    addition_user_ui: function (Id) {
        $.ajax({
            type: "GET",
            url: "/user/addition_user_ui/" + Id,
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    $("#addition_login_label").html(res.data.UserName);

                    $('#addition_account_modal').modal({backdrop: 'static', keyboard: false});
                } else {
                    toastr.error("此用户不存在！");
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    addition_user: function () {
        let addition_amount = $("#addition_amount").val();
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

        if (!patrn2.exec(addition_amount)) {
            toastr.error("请输入正确金额!");
        } else {
            $.ajax({
                type: "POST",
                url: "/user/addition_user/",
                data: {
                    _amount: addition_amount,
                },
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        $("#addition_account_modal").modal('hide');
                        toastr.success("加款成功！");
                        setInterval(function () {
                            window.location.reload();
                        }, 3000);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    deduction_user_ui: function (Id) {
        $.ajax({
            type: "GET",
            url: "/user/deduction_user_ui/" + Id,
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    $("#deduction_label").html(res.data.UserName);

                    $('#deduction_account_modal').modal({backdrop: 'static', keyboard: false});
                } else {
                    toastr.error("此用户不存在！");
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    deduction_user: function () {
        let deduction_amount = $("#deduction__amount").val();
        let patrn2 = /^(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*))$/;

        if (!patrn2.exec(deduction_amount)) {
            toastr.error("请输入正确金额!");
        } else {
            $.ajax({
                type: "POST",
                url: "/user/deduction_user/",
                data: {
                    deductionAmount: deduction_amount,
                },
                cache: false,
                success: function (res) {
                    if (res.code === 9) {
                        $("#deduction_account_modal").modal('hide');
                        toastr.success("减款成功！");
                        setInterval(function () {
                            window.location.reload();
                        }, 3000);
                    } else {
                        toastr.error(res.msg);
                    }
                },
                error: function (XMLHttpRequest) {
                    toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
                }
            });
        }
    },
    look_user: function (lookId) {
        $.ajax({
            type: "GET",
            url: "/user/look_user_ui/" + lookId,
            cache: true,
            success: function (res) {
                if (res.code === 9) {
                    $("#look_userId").html(res.data.u.Id);
                    $("#look_label_name").html(res.data.u.UserName);
                    $("#look_uMobile").html(res.data.u.Mobile);
                    $("#look_uName").html(res.data.u.UserName);
                    $("#look_uAmount").html(res.data.u.UsableAmount);
                    $("#look_uNo").html(res.data.u.Id);
                    $("#look_uApiKey").html(res.data.u.ApiKey);

                    $('#look_pay_fee').html(res.data.u.PayFee);
                    $('#look_recharge_fee').html(res.data.u.RechargeFee);
                    $('#look_user_rate').html(res.data.u.RechargeRate);

                    $('#look_IPs').html(res.data.ux.Ips);

                    $('#look_account_modal').modal({backdrop: 'static', keyboard: false});
                } else {
                    toastr.error("此用户不存在！");
                }
            },
            error: function (XMLHttpRequest) {
                toastr.info('something is wrong, code: ' + XMLHttpRequest.status);
            }
        });
    },
    freeze_user: function (del) {
        swal({
            text: "确定要冻结此用户吗？",
            icon: "warning",
            closeOnClickOutside: false,
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "get",
                    url: "/user/freeze/" + del,
                    success: function (res) {
                        if (res.code === 9) {
                            swal(res.msg, {
                                icon: "success",
                            }).then(() => {
                                setInterval(function () {
                                    window.location.reload();
                                }, 2000);
                            });
                        } else {
                            swal(res.msg, {
                                icon: "warning",
                            });
                        }
                    },
                    error: function (XMLHttpRequest) {
                        swal('something is wrong, code:' + XMLHttpRequest.status, {
                            icon: "warning",
                        });
                    }
                });
            }
        });
    },
    active_user: function (del) {
        swal({
            text: "确定要激活此用户吗？",
            icon: "warning",
            closeOnClickOutside: false,
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "get",
                    url: "/user/active/" + del,
                    success: function (res) {
                        if (res.code === 9) {
                            swal(res.msg, {
                                icon: "success",
                            }).then(() => {
                                setInterval(function () {
                                    window.location.reload();
                                }, 2000);
                            });
                        } else {
                            swal(res.msg, {
                                icon: "warning",
                            });
                        }
                    },
                    error: function (XMLHttpRequest) {
                        swal('something is wrong, code:' + XMLHttpRequest.status, {
                            icon: "warning",
                        });
                    }
                });
            }
        });
    },
    reset_user: function (del) {
        swal({
            text: "确定要将此用户的登录密码重置为默认密码吗？",
            icon: "warning",
            closeOnClickOutside: false,
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: "get",
                    url: "/user/reset/" + del,
                    success: function (res) {
                        if (res.code === 9) {
                            swal(res.msg, {
                                icon: "success",
                            }).then(() => {
                                setInterval(function () {
                                    window.location.reload();
                                }, 2000);
                            });
                        } else {
                            swal(res.msg, {
                                icon: "warning",
                            });
                        }
                    },
                    error: function (XMLHttpRequest) {
                        swal('something is wrong, code:' + XMLHttpRequest.status, {
                            icon: "warning",
                        });
                    }
                });
            }
        });
    },
};