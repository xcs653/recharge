/***************************************************
 ** @Desc : This file for ...
 ** @Time : 2019.04.22 11:04 
 ** @Author : Joker
 ** @File : api_recharge_pay_test.go
 ** @Last Modified by : Joker
 ** @Last Modified time: 2019.04.22 11:04
 ** @Software: GoLand
****************************************************/
package test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"recharge/controllers/implement"
	"recharge/models"
	"strings"
	"testing"
	"time"
)

func TestGenerateSign(t *testing.T) {
	params := models.ApiPayRequestParams{}
	params.MerchantNo = "18"
	params.OrderPrice = "100.01"
	params.OutOrderNo = "2019422115136"
	params.TradeTime = "2019-04-22 11:48:17"
	params.AccountNo = "2019422115136"
	params.AccountName = "是"
	params.ApiNotifyUrl = ""
	params.Item = ""
	params.UserType = "2"
	params.BankNo = "ICBC"
	params.Issuer = "1"
	impl := implement.ApiRechargePayImpl{}
	sign := impl.GenerateSignV1(params, "8O0XXnKCq3vsMiuqZK8XaaiqjlZ6wQ0m5Tir")
	fmt.Println(sign)
}

func TestGenerateSign2(t *testing.T) {
	params := models.ApiQueryParams{}
	params.MerchantNo = "-1"
	params.OutTradeNo = "201904171545399BsGZhTtWH"
	impl := implement.ApiRechargePayImpl{}
	sign := impl.GenerateSignV1(params, "8O0XXnKCq3vsMiuqZK8XaaiqjlZ6wQ0m5Tir")
	fmt.Println(sign)
}

func TestGenerateSign3(t *testing.T) {
	params := models.ApiQueryBalanceParams{}
	params.MerchantNo = "-1"
	params.Timestamp = "20190417154539"
	impl := implement.ApiRechargePayImpl{}
	sign := impl.GenerateSignV1(params, "8O0XXnKCq3vsMiuqZK8XaaiqjlZ6wQ0m5Tir")
	fmt.Println(sign)
}

// 定时任务
func TestTimer(t *testing.T) {
	now := time.Now()
	add := now.Add(time.Second * 2)
	i := 0
	for {

		tm := time.NewTicker(add.Sub(now))

		if i == 4 {
			tm.Stop()
			break
		}
		add = add.Add(time.Second * 4)

		fmt.Println("tm=:", tm)

		k := <-tm.C
		fmt.Println("i:==", i)
		// 23 29 39 53
		fmt.Println("k.Second():===", k.Second())
		i++
	}
}

func TestApiRechargeAsyNotice(t *testing.T) {
	notice := implement.ApiAsyNotice{}
	record := models.RechargeRecord{}
	record.UserId = -1
	record.Remark = "a"
	record.ReOrderId = "12323434334"
	record.SerialNumber = "11111111111111111"
	record.ReAmount = 24
	record.Status = "I"
	record.Item = "24"
	record.EditTime = "432432432423"
	record.ApiNoticeUrl = "http://localhost/gateway/wanka_test/"
	notice.ApiRechargeAsyNotice(record)
}

func TestHttpGet(t *testing.T) {
	resp, err := http.Get("http://127.0.0.1/gateway/scanPayNotify/notifyTest/?Amount=2.00&AmountFee=1.00&AmountNotFee=1.00&MerchantNo=29&Msg=dsa&OutTradeNo=TX00000009105&ResultCode=0000&Sign=A292EFF0403916577DC375DDF920902A&TradeNo=XF20190522002712SVh76OLT1K&TradeStatus=SUCCESS&TradeTime=2019-05-22 00:27:14")
	if err != nil {
		fmt.Println("请求出错！")
	}
	body, err := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	fmt.Println("响应：", string(body))
}

func TestIP(t *testing.T) {
	contains := strings.Contains("47.244.42.101;47.52.115.215", "47.52.115.215")
	fmt.Println(contains)
}
